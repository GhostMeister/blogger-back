<?php

namespace API;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_posts_specify_locale()
    {
        $posts = Post::factory()->times(5)->create();

        $response = $this->get('/api/post',['locale'=>'en']);

        $response->assertStatus(200);
        $response->assertJsonCount(5,'data');
    }

    public function test_get_all_posts_without_specify_locale()
    {
        $posts = Post::factory()->times(5)->create();

        $response = $this->get('/api/post');

        $response->assertSuccessful();
        $response->assertJsonCount(5,'data');
    }

    public function test_create_post(){
        $response = $this->post('/api/post',[
            'title'=> "Ultra",
            'content'=> "abreviation of locale language"
        ]);

        $post = Post::first();
        $response->assertCreated();
        $this->assertEquals("Ultra", $post->title);
        $this->assertEquals("abreviation of locale language", $post->content);
        $this->assertInstanceOf(Post::class, $post);
        $this->assertDatabaseCount(Post::class,1);
    }

    public function test_create_post_with_min_content(){
        $response = $this->post('/api/post',[
            'title'=> "Ultra",
            'content'=> "abrevS"
        ]);

        $response->assertNotFound();
        $this->assertDatabaseCount(Post::class,0);
    }

    public function test_update_post(){
        $posts = Post::factory()->times(5)->create();
        $id = mt_rand(1,5);
        $response = $this->put('/api/post/'.$id,[
            'title'=> "Ultra",
            'content'=> "abreviation of locale language"
        ]);

        $post = Post::find($id);

        $response->assertSuccessful();
        $this->assertEquals("Ultra", $post->title);
        $this->assertEquals("abreviation of locale language", $post->content);
        $this->assertDatabaseCount(Post::class,5);
    }

    public function test_update_post_with_fake_id(){
        $posts = Post::factory()->times(5)->create();
        $id = 6;
        $response = $this->put('/api/post/'.$id,[
            'title'=> "Ultra",
            'content'=> "abreviation of locale language"
        ]);

        $response->assertNotFound();
        $this->assertDatabaseCount(Post::class, 5);
    }

    public function test_delete_post()
    {
        $posts = Post::factory()->times(5)->create();

        $response = $this->delete('/api/post/3');
        $post = Post::find(3);
        $response->assertSuccessful();
        $this->assertEquals(null, $post);
        $this->assertDatabaseCount(Post::class,4);
    }

    public function test_delete_post_with_fake_id()
    {
        $posts = Post::factory()->times(5)->create();

        $response = $this->delete('/api/post/6');

        $response->assertNotFound();
        $this->assertDatabaseCount(Post::class,5);
    }
}
