<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Localization;
use App\Models\Post;
use App\Http\Resources\Post as PostResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * @OA\Get(
     *      path="/post",
     *      operationId="getAllPost",
     *      tags={"Posts"},
     *      summary="Get List of Post",
     *      description="Returns all posts",
     *     @OA\Parameter(
     *         name="locale",
     *         in="header",
     *         required=false,
     *         description="abreviation of locale language 'fr' for french and 'en' for english",
     *         @OA\Schema(
     *             type="string",
     *             format="byte",
     *             example="fr"
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $posts = Post::all();
        return response()->json([
            'success' => true,
            'data' => PostResource::collection($posts),
            'message' => trans('post.fetched')
        ], 200);
    }

    /**
     * @OA\Post(
     *      path="/post",
     *      operationId="CreatePost",
     *      tags={"Posts"},
     *      summary="Create Post",
     *      description="Create a Post by giving a title and content",
     *     @OA\Parameter(
     *         name="locale",
     *         in="header",
     *         required=false,
     *         description="abreviation of locale language 'fr' for french and 'en' for english",
     *         @OA\Schema(
     *             type="string",
     *             format="byte",
     *             example="fr"
     *         )
     *     ),
     *      @OA\RequestBody(
     *      required=true,
     *      description="Pass title and content of your post",
     *      @OA\JsonContent(
     *          required={"title","content"},
     *          @OA\Property(property="title", type="string", format="text", minLength=3, example="Hello Everyone"),
     *          @OA\Property(property="content", type="string", format="text", minLength=10, example="Thanks all of you for this API!")
     *      ),
     * ),
     *     @OA\Response(
     *          response=201,
     *          description="Created",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number",example=1),
     *              @OA\Property(property="title", type="string", minLength=3, example="My first commit"),
     *              @OA\Property(property="content", type="string", minLength=10, example="Welcome to my first commit!"),
     *              @OA\Property(property="updated_at", type="date",example="2022-01-22 05:37:23"),
     *              @OA\Property(property="created_at", type="date",example="2022-01-22 05:37:23")
     *          )
     *     ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      )
     *  )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|string',
            'content' => 'required|min:10'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data' => [],
                'message' => $validator->errors()
            ], 400);
        }

        $post = Post::create($request->all());
        $post->save();

        return response()->json([
            'success' => true,
            'data' => new PostResource($post),
            'message' => trans('post.created')
        ], 201);
    }

    /**
     * @OA\Get(
     *     path="/post/{id}",
     *     tags={"Posts"},
     *     summary="Get Post detail",
     *     description="Show details of a post.For valid response try integer ID with positive integer value. Negative or non-integer values will generate API errors",
     *     operationId="showPost",
     *     @OA\Parameter(
     *         name="locale",
     *         in="header",
     *         required=false,
     *         description="abreviation of locale language 'fr' for french and 'en' for english",
     *         @OA\Schema(
     *             type="string",
     *             format="byte",
     *             example="fr"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Post that needs to be show details",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number",example=1),
     *              @OA\Property(property="title", type="string", minLength=3, example="My first commit"),
     *              @OA\Property(property="content", type="string", minLength=10, example="Welcome to my first commit!"),
     *              @OA\Property(property="updated_at", type="date",example="2022-01-22 05:37:23"),
     *              @OA\Property(property="created_at", type="date",example="2022-01-22 05:37:23")
     *          )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Post not found"
     *     )
     * ),
     */
    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id) :JsonResponse
    {
        try{
            $post = Post::findorFail($id);

            return response()->json([
                'success' => true,
                'data' => $post,
                'message' => trans('post.fetched')
            ], 200);
        }catch(ModelNotFoundException $e){
            return response()->json([
                'success' => false,
                'data' => [],
                'message' => trans('post.not_exist')
            ], 404);
        }
    }

    /**
     * @OA\Put(
     *     path="/post/{id}",
     *     tags={"Posts"},
     *     summary="Update Post",
     *     description="For valid response try integer ID with positive integer value. Negative or non-integer values will generate API errors",
     *     operationId="updatePost",
     *     @OA\Parameter(
     *         name="locale",
     *         in="header",
     *         required=false,
     *         description="abreviation of locale language 'fr' for french and 'en' for english",
     *         @OA\Schema(
     *             type="string",
     *             format="byte",
     *             example="fr"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Post that needs to be updated",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             minimum=1
     *         )
     *     ),
     *     @OA\RequestBody(
     *      required=true,
     *      description="Pass title and content of post need to be updated",
     *      @OA\JsonContent(
     *          required={"title","content"},
     *          @OA\Property(property="title", type="string", format="text", example="Hello Everyone"),
     *          @OA\Property(property="content", type="string", format="text", example="Thanks all of you for this API!")
     *      ),
     * ),
     *     @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number",example=1),
     *              @OA\Property(property="title", type="string", minLength=3, example="My first commit"),
     *              @OA\Property(property="content", type="string", minLength=10, example="Welcome to my first commit!"),
     *              @OA\Property(property="updated_at", type="date",example="2022-01-22 05:37:23"),
     *              @OA\Property(property="created_at", type="date",example="2022-01-22 05:37:23")
     *          )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Post not found"
     *     )
     * ),
     */
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        try{
            $post = Post::findOrFail($id);

            $validator = Validator::make($request->all(), [
                'title' => 'required|min:3|string',
                'content' => 'required|min:10'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'data' => [],
                    'message' => $validator->errors()
                ], 401);
            }

            $post->update($request->all());
            return response()->json([
                'success' => true,
                'data' => new PostResource($post),
                'message' => trans('post.updated')
            ], 200);

        }catch (ModelNotFoundException $e){
            return response()->json([
                'success' => false,
                'data' => $request->all(),
                'message' => trans('post.not_exist')
            ], 404);
        }
    }

    /**
     * @OA\Delete(
     *     path="/post/{id}",
     *     tags={"Posts"},
     *     summary="Delete Post",
     *     description="For valid response try integer ID with positive integer value. Negative or non-integer values will generate API errors",
     *     operationId="deletePost",
     *     @OA\Parameter(
     *         name="locale",
     *         in="header",
     *         required=false,
     *         description="abreviation of locale language 'fr' for french and 'en' for english",
     *         @OA\Schema(
     *             type="string",
     *             format="byte",
     *             example="fr"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Post that needs to be deleted",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="title", type="string", minLength=3, example="My first commit"),
     *              @OA\Property(property="content", type="string", minLength=10, example="Welcome to my first commit!"),
     *              @OA\Property(property="updated_at", type="date", example="2022-01-22 05:37:23"),
     *              @OA\Property(property="created_at", type="date", example="2022-01-22 05:37:23")
     *          )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Post not found"
     *     )
     * ),
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try{
            $post = Post::findOrFail($id);
            $post->delete();

            return response()->json([
                'success' => true,
                'data' => new PostResource($post),
                'message' => trans('post.deleted')
            ], 200);
        }catch(ModelNotFoundException $e){
            return response()->json([
                'success' => false,
                'data' => [],
                'message' => trans('post.not_exist')
            ], 404);
        }

    }
}
