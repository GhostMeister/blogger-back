<?php

return [
    'fetched' => 'Post fetched',
    'updated' => 'Post updated',
    'deleted' => 'Post deleted',
    'created' => 'Post created',
    'not_exist' => 'Post doesn\'t exist!'
];
