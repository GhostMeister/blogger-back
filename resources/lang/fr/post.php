<?php

return [
    'fetched' => 'Article récupéré',
    'updated' => 'Article mis à jour',
    'deleted' => 'Article supprimé',
    'created' => 'Article créé',
    'not_exist' => 'l\'article n\'existe pas!'
];
