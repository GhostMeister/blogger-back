<?php

return [

    'min' => [
        'string' => ':attribute doit comporter au moins :min caractères.'
    ],

    'required' => 'le champ :attribute field est obligatoire.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
